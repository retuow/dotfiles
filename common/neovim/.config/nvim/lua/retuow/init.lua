-- General vim settings
require('retuow.options')

-- Custom/additional key mappings
require('retuow.keymapping')

-- Plugin manager + plugins
-- Plugin configuration can be found in ~/.config/nvim/after/plugin
require('retuow.plugins')
