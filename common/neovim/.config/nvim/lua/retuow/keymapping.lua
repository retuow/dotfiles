-- Also see ~/.config/nvim/after/plugin/*.lua for plugin specific bindings.

-- Remap leader key
vim.g.mapleader = ','

-- Add keymaps to toggle (relative) line numbers
vim.keymap.set('n', '<leader>n', function()
    vim.cmd [[ set number! relativenumber! ]]
end)
