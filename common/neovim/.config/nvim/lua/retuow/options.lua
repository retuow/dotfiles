-- Context
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.scrolloff = 4
vim.opt.cursorline = true
vim.opt.colorcolumn = '80'

-- Encoding
vim.opt.encoding = 'utf8'
vim.opt.fileencoding = 'utf8'

-- Search
vim.opt.ignorecase = false
vim.opt.smartcase = true
vim.opt.incsearch = true
vim.opt.hlsearch = false

-- Whitespace
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 4

-- netrw config
vim.g.netrw_browse_split = 0
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25

-- Disable mouse
vim.opt.mouse = ""
