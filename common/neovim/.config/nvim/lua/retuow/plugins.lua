-- Bootstrap lazy.nvim Plugin Manager
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Plugins
-- Configuration can be found in ~/.config/nvim/after/plugin
require('lazy').setup({
    -- catppuccin color scheme
    { "catppuccin/nvim", name = "catppuccin", priority = 1000 },
    -- improved syntax highlighting
    { 'nvim-treesitter/nvim-treesitter', build = ':TSUpdate' },
    -- additional lua functions used by telescope
    { 'nvim-lua/plenary.nvim' },
    -- telescope fuzzy file searcher
    { 'nvim-telescope/telescope.nvim', branch = '0.1.x' },
    -- telescope fzf-native extension
    { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
})
