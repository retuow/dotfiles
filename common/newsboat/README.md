# zsh dotfiles

Required packages:

* `newsboat`
* `yt-dlp`
* `mpv`

This is mainly used as a backup for when the FreeTube app has issues playing
YouTube videos.
