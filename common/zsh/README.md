# zsh dotfiles

Required packages:

* `zsh`
* `zsh-autosuggestions`
* `zsh-syntax-highlighting`
* `btm`
* `mpv`
* `neovim` (also has dotfiles in this repository)
* `starship` (also has dotfiles in this repository)
