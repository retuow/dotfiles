# Change cursor for different vi modes (like neovim does as well).

CURSOR_BLOCK='\e[2 q'
CURSOR_BEAM='\e[6 q'

function zle-keymap-select() {
    case $KEYMAP in
        vicmd) echo -ne $CURSOR_BLOCK ;;
        viins|main) echo -ne $CURSOR_BEAM ;; # beam
    esac
}
zle -N zle-keymap-select

function zle-line-init() {
    echo -ne $CURSOR_BEAM
}
zle -N zle-line-init
