# History settings
HISTFILE=~/.local/state/zsh/histfile
HISTSIZE=5000
SAVEHIST=5000
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Some basic options and tweaks
setopt autocd
setopt rmstarsilent
unsetopt nomatch

# Keybinds
bindkey -v
bindkey "^a" beginning-of-line
bindkey "^e" end-of-line
bindkey '^j' history-search-forward
bindkey '^k' history-search-backward

# Autocomplete
zmodload zsh/complist
autoload -Uz compinit
compinit
setopt auto_menu
setopt menu_complete

# Colours - completion
eval "$(dircolors)"
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu select
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' squeeze-slashes false

# Colours - manpages with less pager
export MANROFFOPT=-c
export LESS_TERMCAP_mb=$'\e[1;31m'
# Start/stop bold effect
export LESS_TERMCAP_md=$'\e[1;31m'
export LESS_TERMCAP_me=$'\e[0m'
# Start/stop underline effect
export LESS_TERMCAP_us=$'\e[1;4;32m'
export LESS_TERMCAP_ue=$'\e[0m'
# Start/stop stand-out effect
export LESS_TERMCAP_so=$'\e[1;44;33m'
export LESS_TERMCAP_se=$'\e[0m'

# Expand PATH
export PATH=~/.local/bin:$PATH

# Aliases
alias emacs='nvim'
alias view='nvim -R'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias btm='btm --theme=nord'

# Aliases for interactive file management
alias cpi='cp -i'
alias mvi='mv -i'
alias rmi='rm -i'
alias lni='ln -i'

# Audio-only mpv (instead of mpg123, ogg123, flac123, ...)
alias mpv123='mpv --no-video --no-audio-display'

# Prompt customization
export STARSHIP_CONFIG=~/.config/starship/starship.toml
eval "$(starship init zsh)"

# Change cursor for different vi modes.
source ~/.local/share/zsh/zsh-vi-cursor.zsh

# Auto suggestions
bindkey '^ ' autosuggest-accept
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh

# Syntax highlighting
source ~/.local/share/zsh/catppuccin_mocha-zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh
