#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Expand PATH
export PATH=~/.local/bin:$PATH

# Aliases
alias emacs='nvim'
alias view='nvim -R'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias btm='btm --theme=nord'

# Aliases for interactive file management
alias cpi='cp -i'
alias mvi='mv -i'
alias rmi='rm -i'
alias lni='ln -i'

# Audio-only mpv (instead of mpg123, ogg123, flac123, ...)
alias mpv123='mpv --no-video --no-audio-display'

# to fix autocompletion scripts still using the deprecated have
alias have=_have

# Coloured man pages
export MANROFFOPT=-c
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[1;44;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# Bash prompt
export STARSHIP_CONFIG=~/.config/starship/starship.toml
eval "$(starship init bash)"

# Fuzzy Finder
export FZF_DEFAULT_OPTS="\
--color=bg+:#313244,spinner:#f5e0dc,hl:#f38ba8 \
--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
--color=marker:#b4befe,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8 \
--color=selected-bg:#45475a \
--tmux"
eval "$(fzf --bash)"
