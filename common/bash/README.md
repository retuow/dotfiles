# bash dotfiles

Required packages:

* `bash`
* `bash-completion`
* `btm`
* `fzf`
* `mpv`
* `neovim` (also has dotfiles in this repository)
* `starship` (also has dotfiles in this repository)
