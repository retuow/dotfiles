# swaywm dotfiles

Required packages:

* `sway`
* `sway-contrib`
* `swaybg`
* `swayidle`
* `swaylock`
* `swaync`
* `waybar`
* `foot`
* `fuzzel`
* `mpc`
* `mpd`
* `pamixer`
* `pavucontrol`
* `polkit-kde-agent`
* `power-profiles-daemon`
* `wlsunset`
* `ttf-meslo-nerd`

Required dotfiles:

* `common/scripts`
