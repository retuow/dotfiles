# Dotfiles

Dotfiles for my machines. Shared stuff is found in `common`, machine
specific configs in either the `minthara` (laptop) or `mizora` (desktop)
directories.

Dotfiles are managed with the `dotfiles` script. Requires `stow`.

Referenced package names are for Arch Linux.

The Baldur's Gate wallpapers used in Sway are taken from
[Reddit](https://www.reddit.com/r/BaldursGate3/comments/1ay1vlu/hi_guys_i_made_desktop_wallpapers_with_some_npcs/).
